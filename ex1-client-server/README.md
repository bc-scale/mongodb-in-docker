# ex1-client-server

In this example, we create and run a MongoDB server, and cerate and run a
MongoDB client that sends a command to run on the server, which the server
evaluates and returns the result.

Start by positioning a terminal in the root of this example.

```bash
cd ex1-client-server
```

The client and server are defined in docker-compose.yml. Examine this file.
Use the [Docker Compose File Reference](https://docs.docker.com/compose/compose-file/)
to help you understand the syntax of the docker-compose file, the
documentation for the [mongo image](https://hub.docker.com/_/mongo) to understand
what this image offers and how to use it,
documentation for the [mongod command](https://docs.mongodb.com/manual/reference/program/mongod/)
to run a MongoDB server, and documentation for
the [mongo command](https://docs.mongodb.com/manual/mongo/) to run the MongoDB
shell client.

Use [docker-compose](https://docs.docker.com/compose/) to start the server.

```bash
docker-compose up --detach server
```

Inspect the logs.

```bash
docker-compose logs server
```

Look for a lines like the following that indicates that the server is listing to port 27017.

```bash
server_1  | 2020-06-12T16:41:00.008+0000 I  NETWORK  [listener] Listening on /tmp/mongodb-27017.sock
server_1  | 2020-06-12T16:41:00.008+0000 I  NETWORK  [listener] Listening on 0.0.0.0
server_1  | 2020-06-12T16:41:00.009+0000 I  NETWORK  [listener] waiting for connections on port 27017
```

If you don't see them. Re-run `docker-compose logs server` until you do, or you see an error. If you see an error, you'll need to debug that before going on.

Now run the client.

```bash
docker-compose run --rm client
```

You should see output similar to the following.

```bash
MongoDB shell version v4.2.7
connecting to: mongodb://server:27017/?compressors=disabled&gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("6010a13a-f2d5-4af3-b94c-4eadbadfd364") }
MongoDB server version: 4.2.7
test
```

The client asks the server to evaluate the javascript command `db` which reports
the default database it has connected to -- the test database.

Shut down the server, and this example.

```bash
docker-compose down
```

Our example is complete. We can return to the parent directory.

```bash
cd ..
```