# Start with the most recent version of major version 4 of the mongo image.
FROM mongo:4

# Download the wait command from docker-compose-wait and place it in the root
# of our container image.
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait

# Copy our wait-then-run.sh script into the root of our container image.
COPY wait-then-run.sh /wait-then-run.sh

# Make sure both scripts are executable.
RUN chmod +x /wait /wait-then-run.sh

# When the container starts run wait-then-run.sh.
ENTRYPOINT [ "/wait-then-run.sh" ]
