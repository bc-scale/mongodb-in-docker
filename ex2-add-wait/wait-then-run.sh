#!/bin/sh

# Run docker-compose-wait's wait command and then, if successful,
# run whatever is passed to us ("$@"). If the wait fails, we will not run
# what is passed to us.
/wait && "$@"
