# ex4-add-root

New goal: access control. MongoDB can support multiple databases and multiple
clients within the same server. By default any client that is able to connect
to the server can do whatever they want with any database.

In ex3-add-tls, we limited those who can access our server to only those with a
valid certificate. But we may have many client containers on the same network.
If any of them become compromised, they will have access to any database within
our server. Also, it's possible that programming or configuration accidents
may occur which may cause one client to damage or leak the data of another.

So our new goal is to limit the "blast radius" of a security breach by giving
each client access to the databases that it needs access to, and no more.
In security parlance, this is the principle of least privilege (PoLP).

In this example, as a first step to implementing access control, we will create
a root account in our server that has complete control over the server. In a
later example, we'll use this account to create other accounts and databases,
and restrict access to databases based on accounts.

Let's begin.

```bash
cd ex4-add-root
```

Generate TLS files for all of our services. Notice that there are two new
services: unauthenticated and authenticated. We'll discuss them momentarily.

```bash
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server unauthenticated authenticated
```

Examine docker-compose.yml. It defines server, client, unauthenticated,
and authenticated. Server passes the name and the password for the root account using two environment variables: MONGO_INITDB_ROOT_USERNAME and MONGO_INITDB_ROOT_PASSWORD_FILE. Specifically it passes "root" as the root account's username, and it passes the path to a file "/secrets/mongo-initdb-root-password.txt" that contains root's password. The server's volumes section mounts `/secrets` from a subdirectory of the current directory by the same name: `./secrets`. So we need to create the file `./secrets/mongo-initdb-root-password.txt` that holds the root's password. In the commands below you can replace "some-password" with any password you like (note that the quotes are not part of the password).

```bash
mkdir -p secrets
echo "some-password" > secrets/mongo-initdb-root-password.txt
```

With that set and the TLS files generated, we can start the server.

```bash
docker-compose up --detach server
docker-compose logs
```

Now, let's run client.

```bash
docker-compose run --rm client
```

This is the same client as we had before. It is not authenticating itself
as root. You'll notice that it has no problem connecting to the server,
and is able to evaluated "db" which reports the database it connected to
was test. Also note that client did not authenticate. So apparently we can
still connect and perform some operations without being authenticated.
You might think of this as being a "guest" on the server.

Next, run unauthenticated.

```bash
docker-compose run --rm unauthenticated
```

Notice this produces an "Unauthorized" error. This service tried to perform
"db.getCollectionNames()" which it apparently cannot do without being
authenticated.

Next, run authenticated.

```bash
docker-compose run --rm authenticated
```

This time we are prompted for a password (because we didn't provide a value
for the --password option in the docker-compose.yml file; that's a good thing
because we wouldn't want to commit a password to our Git repository).

Enter the same password you placed in the secrets/mongo-init-root-password.txt
file before. Note, when you type the password nothing will appear -- no
asterisks, no characters, nothing. Type the password with confidence and then
press enter. If you make a mistake, press enter, the service will fail, rerun
the service, and try entering the password again. If that fails again, double
check that you are typing the same value that is in
secrets/mongo-init-root-password.txt.

If you entered the correct password for root, you will be rewarded with the
names of the collections in the test database: [ ]. That's right, there aren't
any collections in that database. But we now know that because we were able to
evaluate db.getCollectionNames() because we were authenticated as root.

This ends this example. Let's clean up.

```bash
docker-compose down
rm -r tls secrets
cd ..
```
