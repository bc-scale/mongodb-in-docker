# ex6-security-notes

This is not an example so much as a note on security in MongoDB and Docker.

So far we have implemented encryption-in-motion using TLS and basic access
control. These are good first steps for securing our systems and our databases.
However, there are more steps we can take.

MongoDB has an entire section of their documentation dedicated to
[security](https://docs.mongodb.com/manual/security/). In particular they provide a nice
[security checklist](https://docs.mongodb.com/manual/administration/security-checklist/).
Below is a list of what they suggest and what we have done in the examples so far.

- [x] Enable Access Control and Enforce Authentication
- [x] Configure Role-Based Access Control
- [x] Encrypt Communication (TLS/SSL)
- [ ] Encrypt and Protect Data
- [ ] Limit Network Exposure
- [ ] Audit System Activity
- [ ] Run MongoDB with a Dedicated User
- [ ] Run MongoDB with Secure Configuration Options
- [ ] Request a Security Technical Implementation Guide (where applicable)
- [ ] Consider Security Standards Compliance
- [ ] Periodic/Ongoing Production Checks

As you can see we have only scratched the surface of security in the context
of MongoDB. Some of these may be covered in later examples. But we wanted to
may you aware of the many steps you should be considering before your product
makes it into production.

Similarly Docker's documentation has a [section on security](https://docs.docker.com/engine/security/security/). The Docker community has produced many best
practices articles around Docker, and many of them touch on security.
[This article](https://www.stackrox.com/post/2019/09/docker-security-101/) is
a best practice article focusing on security in Docker.

As we continue our exploration of Docker and MongoDB we will continue to
incorporate security topics as we can. But understand that the topic of security
is broad and deep and requires more attention than we can reasonably provide.
