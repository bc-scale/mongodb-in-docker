# ex9-crud

In this example, we demonstrate some basic CRUD (create, read, update, and delete) operations.

Examine the files in `client/scripts` and `client/lib`.


```bash
cd ex9-crud
mkdir -p secrets
echo "some-password" > secrets/mongo-initdb-root-password.txt
echo "another-password" > secrets/test-owner-password.txt
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server
docker-compose up --detach server
```

After a few seconds, inspect the logs.

```bash
docker-compose logs
```

Now inspect the scripts in `client/scripts`. When you are ready, try them out by running the client and specifying the script you want to run as a command line argument. For example, to run `create.js`...

```bash
docker-compose run --rm client create.js
```

To run `read.js`...

```bash
docker-compose run --rm client read.js
```

Try running each script. To see the results of each script, be sure to run `read.js` between the runs of each script. For example...

```bash
docker-compose run --rm client read.js
docker-compose run --rm client create.js
docker-compose run --rm client read.js
docker-compose run --rm client update.js
...
```

Now let's see what happens when you tear down the server and start it again.

```bash
docker-compose down
docker-compose up --detach server
docker-compose run --rm client read.js
```

Note that the output of read.js is empty. That is, no JSON entries are printed.
This means that the data stored in the MongoDB server is destroyed when the server's container is destroyed. We'll explore this more in our next example.

When you are satisfied, clean up.

```bash
docker-compose down
rm -r tls secrets
cd ..
```